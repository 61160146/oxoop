package com.toy.anagrams.lib.OXGame;


import java.util.Scanner;

public class OXOOP {

    public static void main(String[] args) {
        Scanner kb = new Scanner(System.in);
        Table tableXO = new Table();
        ShowResult print = new ShowResult();
        int endgame = 0;
        int row = 0;
        int col = 0;
        int count = 0;
        int turn = 0;
        int owin = 0;
        int xwin = 0;
        char ans = 'Y';
        print.showWelcome();
        while (true) {
            System.out.println();
            if (ans == 'N') {
                if (owin > xwin) {
                    System.out.println("Finally, Player O is winner!!");
                } else if (xwin > owin) {
                    System.out.println("Finally, Player X is winner!!");
                } else {
                    System.out.println("Finally, It's a tie!!");
                }
                System.out.println("Let's play again next time!!");
                break;
            } else if (ans == 'Y') {
                while (endgame == 0) {
                    tableXO.showTable();
                    if ((owin == xwin && owin%2==0) || owin > xwin) {
                        TurnO.showTurnO(endgame, turn, count, tableXO);
                        count++;
                        if (CheckGame.checkTie(count, tableXO, endgame) > 0) {
                            endgame++;
                        } else if (CheckGame.checkOWinRow(row, col, tableXO, endgame) > 0
                                || CheckGame.checkOWinCol(row, col, tableXO, endgame) > 0
                                || CheckGame.checkOWinX(row, col, tableXO, endgame) > 0
                                && endgame == 0) {
                            endgame++;
                            owin++;
                        } else if (endgame == 0) {
                            tableXO.showTable();
                            TurnX.showTurnX(endgame, turn, count, tableXO);
                            count++;
                            if (CheckGame.checkXWinRow(row, col, tableXO, endgame) > 0
                                    || CheckGame.checkXWinCol(row, col, tableXO, endgame) > 0
                                    || CheckGame.checkXWinX(row, col, tableXO, endgame) > 0
                                    && endgame == 0) {
                                endgame++;
                                xwin++;
                            } else if (CheckGame.checkTie(count, tableXO, endgame) > 0) {
                                endgame++;
                            }
                        }
                    } else {
                        TurnX.showTurnX(endgame, turn, count, tableXO);
                        count++;
                        if (CheckGame.checkTie(count, tableXO, endgame) > 0) {
                            endgame++;
                        } else if (CheckGame.checkXWinRow(row, col, tableXO, endgame) > 0
                                || CheckGame.checkXWinCol(row, col, tableXO, endgame) > 0
                                || CheckGame.checkXWinX(row, col, tableXO, endgame) > 0 && endgame == 0) {
                            endgame++;
                            xwin++;
                        } else if (endgame == 0) {
                            tableXO.showTable();
                            TurnO.showTurnO(endgame, turn, count, tableXO);
                            count++;
                            if (CheckGame.checkOWinRow(row, col, tableXO, endgame) > 0
                                    || CheckGame.checkOWinCol(row, col, tableXO, endgame) > 0
                                    || CheckGame.checkOWinX(row, col, tableXO, endgame) > 0 && endgame == 0) {
                                endgame++;
                                owin++;
                            } else if (CheckGame.checkTie(count, tableXO, endgame) > 0) {
                                endgame++;
                            }
                        }
                    }

                    if (endgame != 0) {
                        System.out.println("\n-------- Score --------");
                        System.out.println("O's score : " + owin);
                        System.out.println("X's score : " + xwin);
                        System.out.println("\nGame is over. Do you want to play again?");
                        System.out.print("Please input Y/N : ");
                        ans = kb.next().charAt(0);
                        if (ans == 'Y') {
                            endgame = 0;
                            count = 0;
                            System.out.println("\n-------- One More!! --------");
                            tableXO.newTable();
                        }
                    }
                }

            } else {
                if (endgame != 0) {
                    System.out.print("Please input Only Y or N : ");
                    ans = kb.next().charAt(0);
                }
            }

        }

    }
}
