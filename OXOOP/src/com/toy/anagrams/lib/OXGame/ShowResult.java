package com.toy.anagrams.lib.OXGame;


public class ShowResult {

    public void showWelcome() {
        String welcome = "-------- Welcome to OX GAME --------";
        System.out.println(welcome);
    }

    public void showErrorCol() {
        System.out.println("Error!! Collum : Please input 1 or 2 or 3 \n");
    }

    public void showErrorRow() {
        System.out.println("Error!! Row : Please input 1 or 2 or 3 \n");
    }

    public void showErrorRC() {
        System.out.println("Error!! Row and Collum : Please input 1 or 2 or 3 \n");
    }

    public void showErrorChosen() {
        System.out.println("it has already been chosen. \n");
    }

    public void showTie() {
        System.out.println("It's a tie.");
    }

    public void showOWin() {
        System.out.println("Player O is winner!!");
    }

    public void showXWin() {
        System.out.println("Player X is winner!!");
    }

}
