package com.toy.anagrams.lib.OXGame;


import java.util.Scanner;

public class TurnX {

    static Scanner kb = new Scanner(System.in);
    static int row;
    static int col;
    static int endgame;
    static int count;
    static int turn;

    public static int showTurnX(int endgame, int turn, int count, Table tableXO) {
        System.out.println("turn X");
        System.out.print("Please input row and collum : ");
        row = kb.nextInt() - 1;
        col = kb.nextInt() - 1;
        insertX(endgame, turn, count, tableXO, row, col);
        return turn = 1;
    }

    public static void insertX(int endgame, int turn, int count, Table tableXO, int row, int col) {
        if (row > 2 || row < 0 || col > 2 || col < 0 || Table.tableOX[row][col] == 'O'
                || Table.tableOX[row][col] == 'X') {
            CheckGame.checkError(row, col, tableXO, endgame);
            showTurnX(endgame, turn, count, tableXO);
        } else {
            tableXO.setX(row, col);
        }
    }
}
