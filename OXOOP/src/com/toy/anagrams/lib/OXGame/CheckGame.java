package com.toy.anagrams.lib.OXGame;


public class CheckGame {
	static int endgame;
	static int row;
	static int col;
	static int count;
	static int turn;
	static int r;
	static int c;
	static ShowResult print = new ShowResult();
        static Table tableXO;
	
	public static int checkError(int row, int col, Table tableXO,int endgame) {
		if ((col > 2 || col < 0) && (row > 2 || row < 0)) {
			print.showErrorRC();
		} else if (row > 2 || row < 0) {
			print.showErrorRow();
		} else if (col > 2 || col < 0) {
			print.showErrorCol();
		} else if (Table.tableOX[row][col]=='O'|| Table.tableOX[row][col]=='X') {
			print.showErrorChosen();
		}
		return endgame;
	}

	public static int checkTie(int count, Table tableXO, int endgame) {
		if (count == 9 && endgame == 0) {
			tableXO.showTable();
			print.showTie();
			endgame++;
		}
		return endgame;
	}
        public static int checkOWinRow(int r, int c, Table tableXO,int endgame) {
		if ((Table.tableOX[row][col] == 'O' && Table.tableOX[row][col+1] == 'O' && Table.tableOX[row][col+2] == 'O')
				|| (Table.tableOX[row+1][col] == 'O' && Table.tableOX[row+1][col+1] == 'O' &&Table.tableOX[row+1][col+2] == 'O')
				|| (Table.tableOX[row+2][col] == 'O' && Table.tableOX[row+2][col+1] == 'O' && Table.tableOX[row+2][col+2] == 'O')) {
			tableXO.showTable();
			print.showOWin();
			endgame++;
		}
		return endgame;
	}
        public static int checkOWinCol(int r, int c, Table tableXO,int endgame) {
		if ((Table.tableOX[row][col] == 'O' && Table.tableOX[row+1][col] == 'O' && Table.tableOX[row+2][col] == 'O')
				|| (Table.tableOX[row][col+1] == 'O' && Table.tableOX[row+1][col+1] == 'O' &&Table.tableOX[row+2][col+1] == 'O')
				|| (Table.tableOX[row][col+2] == 'O' && Table.tableOX[row+1][col+2] == 'O' && Table.tableOX[row+2][col+2] == 'O')) {
			tableXO.showTable();
			print.showOWin();
			endgame++;
		}
		return endgame;
	}
        public static int checkOWinX(int r, int c, Table tableXO,int endgame) {
		if ((Table.tableOX[0][0] == 'O' && Table.tableOX[1][1] == 'O' && Table.tableOX[2][2] == 'O')
			|| (Table.tableOX[0][2] == 'O' && Table.tableOX[1][1] == 'O' && Table.tableOX[2][0] == 'O')) {
			tableXO.showTable();
			print.showOWin();
			endgame++;
		}
		return endgame;
	}
        public static int checkXWinRow(int r, int c, Table tableXO,int endgame) {
		if ((Table.tableOX[row][col] == 'X' && Table.tableOX[row][col+1] == 'X' && Table.tableOX[row][col+2] == 'X')
				|| (Table.tableOX[row+1][col] == 'X' && Table.tableOX[row+1][col+1] == 'X' &&Table.tableOX[row+1][col+2] == 'X')
				|| (Table.tableOX[row+2][col] == 'X' && Table.tableOX[row+2][col+1] == 'X' && Table.tableOX[row+2][col+2] == 'X')) {
			tableXO.showTable();
			print.showXWin();
			endgame++;
		}
		return endgame;
	}
        public static int checkXWinCol(int r, int c, Table tableXO,int endgame) {
		if ((Table.tableOX[row][col] == 'X' && Table.tableOX[row+1][col] == 'X' && Table.tableOX[row+2][col] == 'X')
				|| (Table.tableOX[row][col+1] == 'X' && Table.tableOX[row+1][col+1] == 'X' &&Table.tableOX[row+2][col+1] == 'X')
				|| (Table.tableOX[row][col+2] == 'X' && Table.tableOX[row+1][col+2] == 'X' && Table.tableOX[row+2][col+2] == 'X')) {
			tableXO.showTable();
			print.showXWin();
			endgame++;
		}
		return endgame;
	}
        public static int checkXWinX(int r, int c, Table tableXO,int endgame) {
		if ((Table.tableOX[0][0] == 'X' && Table.tableOX[1][1] == 'X' && Table.tableOX[2][2] == 'X')
			|| (Table.tableOX[0][2] == 'X' && Table.tableOX[1][1] == 'X' && Table.tableOX[2][0] == 'X')) {
			tableXO.showTable();
			print.showOWin();
			endgame++;
		}
		return endgame;
	}

	

}
