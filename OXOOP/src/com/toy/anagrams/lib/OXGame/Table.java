package com.toy.anagrams.lib.OXGame;

public class Table {

    static char[][] tableOX = {{'-', '-', '-'}, {'-', '-', '-'}, {'-', '-', '-'}};
    static int row;
    static int col;
    static char XO;
    static char X;
    static char O;
    boolean check = false;

    public void setX(int row, int col) {
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                tableOX[row][col] = 'X';
            }
        }
    }

    public void setO(int row, int col) {
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                tableOX[row][col] = 'O';
            }
        }

    }

    public void setTableOX(char[][] tableOX) {
        Table.tableOX = tableOX;
    }

    public void showTable() {
        for (int i = 0; i < 3; i++) {
            System.out.println("+---+---+---+");
            for (int j = 0; j < 3; j++) {
                System.out.print("| ");
                System.out.print(tableOX[i][j]);
                System.out.print(" ");
            }
            System.out.print("| ");
            System.out.println();
        }
        System.out.println("+---+---+---+");
    }

    public void newTable() {
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                tableOX[i][j] = '-';
            }
        }

    }

    public int checkError(int row, int col, Table tableXO, int endgame) {
        if ((col > 2 || col < 0) && (row > 2 || row < 0)) {
        } else if (row > 2 || row < 0) {
        } else if (col > 2 || col < 0) {
        } else if (Table.tableOX[row][col] == 'O' || Table.tableOX[row][col] == 'X') {
        }
        return endgame;
    }

    public boolean checkTie(int count, Table tableXO, int endgame) {
        if (count == 9 && endgame == 0) {
            tableXO.showTable();
            endgame++;
        }
        return true;
    }

    public boolean checkOWinRow() {
        if ((Table.tableOX[row][col] == 'O' && Table.tableOX[row][col + 1] == 'O' && Table.tableOX[row][col + 2] == 'O')
                || (Table.tableOX[row + 1][col] == 'O' && Table.tableOX[row + 1][col + 1] == 'O' && Table.tableOX[row + 1][col + 2] == 'O')
                || (Table.tableOX[row + 2][col] == 'O' && Table.tableOX[row + 2][col + 1] == 'O' && Table.tableOX[row + 2][col + 2] == 'O')) {
             check = true;
        }
        return check;
    }

    public boolean checkOWinCol() {
        if ((Table.tableOX[row][col] == 'O' && Table.tableOX[row + 1][col] == 'O' && Table.tableOX[row + 2][col] == 'O')
                || (Table.tableOX[row][col + 1] == 'O' && Table.tableOX[row + 1][col + 1] == 'O' && Table.tableOX[row + 2][col + 1] == 'O')
                || (Table.tableOX[row][col + 2] == 'O' && Table.tableOX[row + 1][col + 2] == 'O' && Table.tableOX[row + 2][col + 2] == 'O')) {
            check = true;
        }
        return check;
    }

    public boolean checkOWinX() {
        if ((Table.tableOX[0][0] == 'O' && Table.tableOX[1][1] == 'O' && Table.tableOX[2][2] == 'O')
                || (Table.tableOX[0][2] == 'O' && Table.tableOX[1][1] == 'O' && Table.tableOX[2][0] == 'O')) {
            check = true;
        }
        return check;
    }

    public boolean checkXWinRow() {
        if ((Table.tableOX[row][col] == 'X' && Table.tableOX[row][col + 1] == 'X' && Table.tableOX[row][col + 2] == 'X')
                || (Table.tableOX[row + 1][col] == 'X' && Table.tableOX[row + 1][col + 1] == 'X' && Table.tableOX[row + 1][col + 2] == 'X')
                || (Table.tableOX[row + 2][col] == 'X' && Table.tableOX[row + 2][col + 1] == 'X' && Table.tableOX[row + 2][col + 2] == 'X')) {
             check = true;
        }
        return check;
    }

    public boolean checkXWinCol() {
        if ((Table.tableOX[row][col] == 'X' && Table.tableOX[row + 1][col] == 'X' && Table.tableOX[row + 2][col] == 'X')
                || (Table.tableOX[row][col + 1] == 'X' && Table.tableOX[row + 1][col + 1] == 'X' && Table.tableOX[row + 2][col + 1] == 'X')
                || (Table.tableOX[row][col + 2] == 'X' && Table.tableOX[row + 1][col + 2] == 'X' && Table.tableOX[row + 2][col + 2] == 'X')) {
            check = true;
        }
        return check;
    }

    public boolean checkXWinX() {
        if ((Table.tableOX[0][0] == 'X' && Table.tableOX[1][1] == 'X' && Table.tableOX[2][2] == 'X')
                || (Table.tableOX[0][2] == 'X' && Table.tableOX[1][1] == 'X' && Table.tableOX[2][0] == 'X')) {
            check = true;
        }
        return check;
    }
}
